package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp() {
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName() {
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponDescription() {
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue() {
        //TODO: Complete me
    }
}
