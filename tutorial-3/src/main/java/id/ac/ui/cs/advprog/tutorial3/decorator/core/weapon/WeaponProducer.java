package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public enum WeaponProducer {
    WEAPON_GUN,
    WEAPON_LONGBOW,
    WEAPON_SHIELD,
    WEAPON_SWORD;

    public Weapon createWeaponEnhancer() {

        Weapon weapon = null;

        if (this == WeaponProducer.WEAPON_SWORD) {
            weapon = new Sword();
        } else if (this == WeaponProducer.WEAPON_GUN) {
            weapon = new Gun();
        } else if (this == WeaponProducer.WEAPON_LONGBOW) {
            weapon = new Longbow();
        } else if (this == WeaponProducer.WEAPON_SHIELD) {
            weapon = new Shield();
        } else {
            weapon = new Sword();
        }

        return weapon;
    }
}
