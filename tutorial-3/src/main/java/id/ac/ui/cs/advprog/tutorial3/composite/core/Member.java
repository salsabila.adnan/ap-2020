package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.List;

public interface Member {
    public String getName();

    public String getRole();

    public void addChildMember(Member member);

    public void removeChildMember(Member member);

    public List<Member> getChildMembers();
}
