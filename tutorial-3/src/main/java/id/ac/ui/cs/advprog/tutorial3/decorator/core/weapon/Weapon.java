package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public abstract class Weapon {

    protected String weaponName;
    protected String weaponDescription;
    protected int weaponValue;

    public String getName() {
        return weaponName;
    }

    public int getWeaponValue() {
        return weaponValue;
    }

    public String getDescription() {
        return weaponDescription;
    }

}
