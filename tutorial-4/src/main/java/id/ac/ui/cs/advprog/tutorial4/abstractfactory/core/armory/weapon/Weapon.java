package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public interface Weapon {

    String getName();

    String getDescription();
}
